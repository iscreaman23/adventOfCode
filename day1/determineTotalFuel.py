import math

def calculateFuel(mass):
    return math.floor(mass/3)-2

moduleFuel = 0
uncorrectedTotalFuel = 0
totalFuel = 0

with open("moduleMasses", "r") as moduleMasses:
    for moduleMass in moduleMasses:
        moduleFuel = calculateFuel(int(moduleMass))
        uncorrectedTotalFuel += moduleFuel
        fuelForFuel = calculateFuel(moduleFuel)
        while fuelForFuel >= 0:
            moduleFuel += fuelForFuel
            fuelForFuel = calculateFuel(fuelForFuel)
        totalFuel += moduleFuel

print("Uncorrected total fuel :   " + str(uncorrectedTotalFuel) + " units")
print("Corrected total fuel   :   " + str(totalFuel) + " units")
