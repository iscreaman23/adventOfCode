import numpy

class wire:
    def __init__(self,filename,lineNumber):
        self.turnCoords=self.getTurnCoordinates(filename,lineNumber)
        self.wireLabel=lineNumber+1

    def getTurnCoordinates(self,filename,i):
        with open(filename, 'r') as data:
            self.input=data.readlines()[i].split(',')
        coordList=[]
        x=0
        y=0
        totalDist=0
        for segment in self.input:
            direction=segment[0]
            dist=int(segment[1:])
            if direction=="R":
                x+=dist
            elif direction=="L":
                x-=dist
            elif direction=="U":
                y+=dist
            elif direction=="D":
                y-=dist
            coordList.append((x,y,totalDist))
            totalDist+=dist
        self.totalLen=totalDist
        self.nTurns=len(self.input)
        return coordList
