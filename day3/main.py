from wire import wire
from findIntersections import findIntersections
from writeOutput import writeOutput

wire1=wire('data',0)
wire2=wire('data',1)
intersections=findIntersections(wire1.turnCoords,wire2.turnCoords)
writeOutput(wire1,wire2,intersections)
