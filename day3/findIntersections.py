def findIntersections(coords1,coords2):
    intersections=[]
    x1=0
    y1=0
    X1=0
    Y1=0
    for x2,y2,d in coords1:
        for X2,Y2,D in coords2:
            if x1==x2 and isBetween(X1,x1,X2) and isBetween(y1,Y1,y2):
                intersections.append((abs(x1)+abs(Y1),x1,Y1,d+D+abs(y1-Y1)+abs(X1-x1)))
            if X1==X2 and isBetween(x1,X1,x2) and isBetween(Y1,y1,Y2):
                intersections.append((abs(X1)+abs(y1),X1,y1,d+D+abs(y1-Y1)+abs(X1-x1)))
            X1=X2
            Y1=Y2
        x1=x2
        y1=y2
    return intersections

def isBetween(a,b,c):
    if((b>a and b<c) or (b<a and b>c)):
        return True
    else:
        return False
