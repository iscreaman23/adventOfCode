import numpy

desiredOutput=19690720
filename="data"

def loadInput():
    with open(filename, "r") as data:
        return numpy.fromstring(data.readlines()[0], dtype=int, sep=',')

def parseData(input1, input2):
    dataArray=loadInput()
    dataArray[1]=input1
    dataArray[2]=input2
    k=0;
    while dataArray[k]!=99:
        if dataArray[k]==1:
            dataArray[dataArray[k+3]]=dataArray[dataArray[k+1]]+dataArray[dataArray[k+2]]
        elif dataArray[k]==2:
            dataArray[dataArray[k+3]]=dataArray[dataArray[k+1]]*dataArray[dataArray[k+2]]
        else:
            print("Error! " + str(dataArray[k])+" is an invalid opcode!")
        k+=4
    return dataArray[0]

def findNounAndVerb(output):
    for i in range(99):
        for j in range(99):
            if parseData(i,j)==output:
                return i, j

noun, verb = findNounAndVerb(desiredOutput)
print("100 * noun + verb = "+str(100*noun+verb))
